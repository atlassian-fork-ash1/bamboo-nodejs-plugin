package com.atlassian.bamboo.plugins.nodejs.tasks.npm;

import com.atlassian.bamboo.plugins.nodejs.tasks.AbstractNodeRequiringTaskExporter;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.builders.task.NpmTask;
import com.atlassian.bamboo.specs.model.task.NpmTaskProperties;
import com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NpmTaskExporter extends AbstractNodeRequiringTaskExporter<NpmTaskProperties, NpmTask> {
    @Autowired
    public NpmTaskExporter(UIConfigSupport uiConfigSupport) {
        super(NpmTaskProperties.class, uiConfigSupport);
    }

    @NotNull
    @Override
    protected ValidationContext getValidationContext() {
        return NpmTaskProperties.VALIDATION_CONTEXT;
    }

    @NotNull
    @Override
    protected Map<String, String> toTaskConfiguration(@NotNull NpmTaskProperties taskProperties) {
        final Map<String, String> config = new HashMap<>();
        config.put(NpmConfigurator.COMMAND, taskProperties.getCommand());
        config.put(NpmConfigurator.ISOLATED_CACHE, Boolean.toString(taskProperties.isUseIsolatedCache()));
        return config;
    }

    @NotNull
    @Override
    public NpmTask toSpecsEntity(@NotNull Map<String, String> taskConfiguration) {
        return new NpmTask()
                .command(taskConfiguration.get(NpmConfigurator.COMMAND))
                .useIsolatedCache(Boolean.parseBoolean(taskConfiguration.getOrDefault(NpmConfigurator.ISOLATED_CACHE, null)));
    }

    @NotNull
    @Override
    protected List<ValidationProblem> validate(@NotNull NpmTaskProperties taskProperties) {
        final List<ValidationProblem> validationProblems = new ArrayList<>();
        if (StringUtils.isEmpty(taskProperties.getCommand())) {
            validationProblems.add(new ValidationProblem(getValidationContext(), "Command can't be empty"));
        }
        return validationProblems;
    }
}
