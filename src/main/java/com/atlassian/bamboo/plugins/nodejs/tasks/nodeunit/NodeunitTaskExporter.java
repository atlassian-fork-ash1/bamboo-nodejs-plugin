package com.atlassian.bamboo.plugins.nodejs.tasks.nodeunit;

import com.atlassian.bamboo.plugins.nodejs.tasks.AbstractNodeRequiringTaskExporter;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.builders.task.NodeunitTask;
import com.atlassian.bamboo.specs.model.task.NodeunitTaskProperties;
import com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class NodeunitTaskExporter extends AbstractNodeRequiringTaskExporter<NodeunitTaskProperties, NodeunitTask> {
    @Autowired
    public NodeunitTaskExporter(UIConfigSupport uiConfigSupport) {
        super(NodeunitTaskProperties.class, uiConfigSupport);
    }

    @NotNull
    @Override
    protected ValidationContext getValidationContext() {
        return NodeunitTaskProperties.VALIDATION_CONTEXT;
    }

    @NotNull
    @Override
    public Map<String, String> toTaskConfiguration(@NotNull NodeunitTaskProperties taskProperties) {
        final Map<String, String> config = new HashMap<>();
        config.put(NodeunitConfigurator.NODEUNIT_RUNTIME, taskProperties.getNodeunitExecutable());
        config.put(NodeunitConfigurator.TEST_FILES, taskProperties.getTestFilesAndDirectories());
        config.put(NodeunitConfigurator.TEST_RESULTS_DIRECTORY, taskProperties.getTestResultsDirectory());
        config.put(NodeunitConfigurator.PARSE_TEST_RESULTS, Boolean.toString(taskProperties.isParseTestResults()));
        config.put(NodeunitConfigurator.ARGUMENTS, taskProperties.getArguments());
        return config;
    }

    @NotNull
    @Override
    protected NodeunitTask toSpecsEntity(@NotNull Map<String, String> taskConfiguration) {
        return new NodeunitTask()
                .nodeunitExecutable(taskConfiguration.get(NodeunitConfigurator.NODEUNIT_RUNTIME))
                .testFilesAndDirectories(taskConfiguration.get(NodeunitConfigurator.TEST_FILES))
                .testResultsDirectory(taskConfiguration.get(NodeunitConfigurator.TEST_RESULTS_DIRECTORY))
                .parseTestResults(Boolean.parseBoolean(taskConfiguration.getOrDefault(NodeunitConfigurator.PARSE_TEST_RESULTS,
                        Boolean.toString(NodeunitTask.DEFAULT_PARSE_TEST_RESULTS))))
                .arguments(taskConfiguration.getOrDefault(NodeunitConfigurator.ARGUMENTS, null));
    }

    @NotNull
    @Override
    public List<ValidationProblem> validate(@NotNull NodeunitTaskProperties taskProperties) {
        final List<ValidationProblem> validationProblems = new ArrayList<>();

        if (StringUtils.isBlank(taskProperties.getNodeunitExecutable())) {
            validationProblems.add(new ValidationProblem(getValidationContext(), "Nodeunit executable is not defined"));
        }
        if (StringUtils.isBlank(taskProperties.getTestFilesAndDirectories())) {
            validationProblems.add(new ValidationProblem(getValidationContext(), "Test files and/or directories are not defined"));
        }
        if (StringUtils.isBlank(taskProperties.getTestResultsDirectory())) {
            validationProblems.add(new ValidationProblem(getValidationContext(), "Test results directory is not defined"));
        }
        return validationProblems;
    }
}
