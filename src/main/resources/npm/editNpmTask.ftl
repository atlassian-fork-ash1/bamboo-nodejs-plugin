[#-- @ftlvariable name="uiConfigBean" type="com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport" --]

[#assign addExecutableLink][@ui.displayAddExecutableInline executableKey='node' /][/#assign]
[@s.select cssClass="builderSelectWidget" labelKey='node.runtime' name='runtime'
list=uiConfigBean.getExecutableLabels('node')
extraUtility=addExecutableLink required=true /]

[@s.textfield labelKey='npm.command' name='command' cssClass="long-field" required=true /]

[@ui.bambooSection titleKey='repository.advanced.option' collapsible=true
isCollapsed=!(isolatedCache?has_content || environmentVariables?has_content || workingSubDirectory?has_content)]
    [@s.checkbox labelKey='npm.isolatedCache' name='isolatedCache' /]
    [@s.textfield labelKey='builder.common.env' name='environmentVariables' cssClass="long-field" /]
    [@s.textfield labelKey='builder.common.sub' name='workingSubDirectory' cssClass="long-field" /]
[/@ui.bambooSection]