package com.atlassian.bamboo.plugins.nodejs.tasks.mocha.runner;

import com.atlassian.bamboo.plugins.nodejs.tasks.AbstractNodeRequiringTaskConfigurator;
import com.atlassian.bamboo.plugins.nodejs.tasks.node.NodeCapabilityDefaultsHelper;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.builders.task.MochaRunnerTask;
import com.atlassian.bamboo.specs.model.task.MochaRunnerTaskProperties;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskContainer;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskDefinitionImpl;
import com.atlassian.bamboo.task.export.TaskValidationContext;
import com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport;
import com.google.common.collect.ImmutableMap;
import org.jetbrains.annotations.NotNull;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MochaRunnerTaskExporterTest {
    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private UIConfigSupport uiConfigSupport;

    @InjectMocks
    private MochaRunnerTaskExporter mochaRunnerTaskExporter;

    @Test
    public void testToTaskConfiguration() {
        // provided
        final String nodeExecutable = "Node.js 6.0";
        final String mochaExecutable = "custom/node_modules/mochaRunner";
        final String testFilesAndDirectories = "test.js tests/";
        final boolean parseTestResults = true;
        final String arguments = "--run-fast";
        final String environmentVariables = "NODE_HOME=/env/home";
        final String workingSubdirectory = "plugin";

        final TaskContainer taskContainer = mock(TaskContainer.class);

        final MochaRunnerTask mochaRunnerTask = new MochaRunnerTask()
                .nodeExecutable(nodeExecutable)
                .mochaExecutable(mochaExecutable)
                .testFilesAndDirectories(testFilesAndDirectories)
                .parseTestResults(parseTestResults)
                .arguments(arguments)
                .environmentVariables(environmentVariables)
                .workingSubdirectory(workingSubdirectory);
        final MochaRunnerTaskProperties mochaRunnerTaskProperties = EntityPropertiesBuilders.build(mochaRunnerTask);

        // when
        final Map<String, String> configuration = mochaRunnerTaskExporter.toTaskConfiguration(taskContainer, mochaRunnerTaskProperties);

        // then
        assertThat(configuration.get(AbstractNodeRequiringTaskConfigurator.NODE_RUNTIME), is(nodeExecutable));
        assertThat(configuration.get(MochaRunnerConfigurator.MOCHA_RUNTIME), is(mochaExecutable));
        assertThat(configuration.get(MochaRunnerConfigurator.TEST_FILES), is(testFilesAndDirectories));
        assertThat(configuration.get(MochaRunnerConfigurator.PARSE_TEST_RESULTS), is(Boolean.toString(parseTestResults)));
        assertThat(configuration.get(MochaRunnerConfigurator.ARGUMENTS), is(arguments));
        assertThat(configuration.get(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES), is(environmentVariables));
        assertThat(configuration.get(TaskConfigConstants.CFG_WORKING_SUBDIRECTORY), is(workingSubdirectory));
    }

    @Test
    public void testToSpecsEntity() {
        // provided
        final String nodeExecutable = "Node.js 6.0";
        final String mochaExecutable = "custom/node_modules/mochaRunner";
        final String testFilesAndDirectories = "test.js tests/";
        final boolean parseTestResults = true;
        final String arguments = "--run-fast";
        final String environmentVariables = "NODE_HOME=/env/home";
        final String workingSubdirectory = "plugin";

        final Map<String, String> configuration = ImmutableMap.<String, String>builder()
                .put(AbstractNodeRequiringTaskConfigurator.NODE_RUNTIME, nodeExecutable)
                .put(MochaRunnerConfigurator.MOCHA_RUNTIME, mochaExecutable)
                .put(MochaRunnerConfigurator.TEST_FILES, testFilesAndDirectories)
                .put(MochaRunnerConfigurator.PARSE_TEST_RESULTS, Boolean.toString(parseTestResults))
                .put(MochaRunnerConfigurator.ARGUMENTS, arguments)
                .put(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES, environmentVariables)
                .put(TaskConfigConstants.CFG_WORKING_SUBDIRECTORY, workingSubdirectory)
                .build();
        final TaskDefinition taskDefinition = new TaskDefinitionImpl(100L, "pluginKey", null, true, configuration, false);

        // when
        final MochaRunnerTaskProperties taskProperties = EntityPropertiesBuilders.build(mochaRunnerTaskExporter.toSpecsEntity(taskDefinition));

        // then
        assertThat(taskProperties.getNodeExecutable(), is(nodeExecutable));
        assertThat(taskProperties.getMochaExecutable(), is(mochaExecutable));
        assertThat(taskProperties.getTestFilesAndDirectories(), is(testFilesAndDirectories));
        assertThat(taskProperties.isParseTestResults(), is(parseTestResults));
        assertThat(taskProperties.getArguments(), is(arguments));
        assertThat(taskProperties.getEnvironmentVariables(), is(environmentVariables));
        assertThat(taskProperties.getWorkingSubdirectory(), is(workingSubdirectory));
    }

    @Test
    public void testValidatePassesForValidProperties() {
        // provided
        final TaskValidationContext taskValidationContext = mock(TaskValidationContext.class);
        final String[] knownNodeExecutables = {"Node.js 0.11", "Node.js 0.12"};

        final MochaRunnerTask mochaRunnerTask = new MochaRunnerTask().nodeExecutable(knownNodeExecutables[0]);
        final MochaRunnerTaskProperties mochaRunnerTaskProperties = EntityPropertiesBuilders.build(mochaRunnerTask);
        registerKnownServerExecutables(knownNodeExecutables);

        // when
        final List<ValidationProblem> validationProblems = mochaRunnerTaskExporter.validate(taskValidationContext, mochaRunnerTaskProperties);

        // then
        assertThat(validationProblems, is(empty()));
    }

    @Test
    public void testValidateFailsForUnknownExecutable() {
        // provided
        final TaskValidationContext taskValidationContext = mock(TaskValidationContext.class);
        final String[] knownNodeExecutables = {"Node.js 0.11", "Node.js 0.12"};
        final String missingNodeExecutable = knownNodeExecutables[0] + " (missing)";
        final MochaRunnerTask mochaRunnerTask = new MochaRunnerTask().nodeExecutable(missingNodeExecutable);
        final MochaRunnerTaskProperties mochaRunnerTaskProperties = EntityPropertiesBuilders.build(mochaRunnerTask);
        registerKnownServerExecutables(knownNodeExecutables);

        // when
        final List<ValidationProblem> validationProblems = mochaRunnerTaskExporter.validate(taskValidationContext, mochaRunnerTaskProperties);

        // then
        assertThat(validationProblems, hasSize(1));
    }

    private void registerKnownServerExecutables(@NotNull String... labels) {
        when(uiConfigSupport.getExecutableLabels(NodeCapabilityDefaultsHelper.NODE_CAPABILITY_KEY)).thenReturn(Arrays.asList(labels));
    }
}
